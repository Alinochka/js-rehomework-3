let x = +prompt("Enter the number","");

while(x <= 0 || isNaN(x) || x == null){
    x = prompt("Enter the correct number","1")
}

function factorial (x) {
    let result = 1;
    for (let i = 1; i <= x; i++){
        result *= i;
    }
    return result;
}

console.log(factorial(x));
